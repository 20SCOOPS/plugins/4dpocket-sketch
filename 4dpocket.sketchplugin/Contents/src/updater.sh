#!/bin/sh

# u update plugin
# c check if have tools if not download it
# t update tools

update()
{
    git pull
    return 0
}
check()
{
    git fetch

    UPSTREAM='@{u}'
    LOCAL=$(git rev-parse @)
    REMOTE=$(git rev-parse "$UPSTREAM")
    BASE=$(git merge-base @ "$UPSTREAM")

    git reset --hard >/dev/null
    if [ $LOCAL = $REMOTE ]; then
# no update
        return 0
    elif [ $LOCAL = $BASE ]; then
# have update
        return 1
    else
# error
        return -1
    fi
}
clone()
{
    echo clone new contents
    mv ../Contents ../Contents.temp
    git clone https://gitlab.com/20SCOOPS/plugins/sketch-plugins.git ../Contents
    if [ -d "../Contents" ]
    then
        rm -rf ../Contents.temp
    fi
}

optional=${1:-""} 
if ! [ -d ".git" ]; then clone; fi
if [[ $optional = *"u"* ]]
then update
else check
fi
result=$?
echo $result
exit $result