@import 'dha-cmd.js'
@import 'dha-helper.js'
@import 'dha-loader.js'
@import 'dha-json-manipulate.js'

var emptyStateString = "URL or File Path of CSV file"
var fileTemp = "hyky.temp.json"
var config = "src/config.json"

var onRun = context => {
    if (checkNoSelectoion(context)) return
    DHAHelper.log(context, "Please enter csv source url or path.")
    if (!loader(context)) return
    DHAHelper.log(context, "Please select data set.")
    jsonManipulate(context)
}

var checkNoSelectoion = context => {
    if (context.selection.count() > 0) return false
    DHAHelper.alert("Please select something!",
        (v) => { },
        (v) => { },
        null
    )
    return true
}

var loader = context => {
    
    var info = DHALoader.getInfo(context)
    var source = context.document.askForUserInput_initialValue_("Generate language data for this CSV?", info.source || emptyStateString)
    while (source == emptyStateString) {
        source = context.document.askForUserInput_initialValue_(`Please Enter "${emptyStateString}"!!!`, source)
    }
    if (!source) {
        DHAHelper.log(context, "Cancel")
        return false
    }
    DHAHelper.log(context, "Run Script HYKY")
    try {
        var tempPath = DHAHelper.rootDir(context, fileTemp)
        var path = DHAHelper.pluginDir(context)
        log(path)
        log(tempPath)
        log(DHAHelper.docDir(context))
        var logs1 = DHACommand.tools(path,"konyak","csv", source, tempPath)
        log(">>"+logs1)
        var logs2 = DHACommand.tools(path,"honyak","sketch", tempPath, DHAHelper.docDir(context),DHAHelper.pluginDir(context,config))
        log(">>"+logs2)

        DHACommand.remove(DHAHelper.pluginDir(context),tempPath)

        info.source = source
        DHALoader.saveNewInfo(context, info)
        DHAHelper.log(context, "Done.")
        return true
    } catch (error) {
        DHAHelper.log(context, error)
        return false
    }
}

var jsonManipulate = context => {
    if (checkNoSelectoion(context)) return
    var info = DHALoader.getInfo(context)
    DHAHelper.alertDropdown(
        context.command.name(),
        info.items || [],
        (view, dropdown) => {
            //will show
        },
        (view, dropdown, result, index) => {
            //ok
            var dataSetName = info.items[index].name
            DHAHelper.log(context, ("Load data set => " + result))
            DHAJSONManipulate.settingNodes(
                DHAJSONManipulate.loadDataSet(context, info.items[index].name),
                context.selection
            )
            info.lastSelecting = dataSetName
            DHALoader.saveNewInfo(context, info)
            DHAHelper.log(context, "Done")
            return true
        },
        (view, dropdown) => {
            //cancel
            DHAHelper.log(context, "Cancel")
            return false
        }
    )
}