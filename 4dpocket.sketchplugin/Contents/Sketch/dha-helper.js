var DHAHelper = {
    /**
     * append directory with path
     * @param {String} dir
     * @param {String} path
     * @return {String} full path
     */
    dir: (dir, path) => {
        return path && dir.stringByAppendingPathComponent_(path) || dir
    },
    /**
     * script directory
     * @param {Context} context
     * @param {String} path
     * @return {String} directory path
     */
    scriptDir: (context, path) => {
        return DHAHelper.dir(context.scriptPath.stringByDeletingLastPathComponent(), path)
    },
    /**
     * plugin directory
     * @param {Context} context
     * @param {String} path
     * @return {String} directory path
     */
    pluginDir: (context, path) => {
        return DHAHelper.dir(DHAHelper.scriptDir(context).stringByDeletingLastPathComponent(), path)
    },
    /**
     * root directory
     * @param {Context} context
     * @param {String} path
     * @return {String} directory path
     */
    rootDir: (context, path) => {
        return DHAHelper.dir(DHAHelper.pluginDir(context).stringByDeletingLastPathComponent(), path)
    },
    /**
     * document directory
     * @param {Context} context
     * @param {String} path
     * @return {String} document directory path
     */
    docDir: (context, path) => {
        return DHAHelper.dir(
            DHAHelper.dir(
                DHAHelper.rootDir(context, "doc"),
                context.document.fileURL().path().stringByDeletingPathExtension().lastPathComponent()
            ),
            path
        )
    },
    loadJSON: filePath => {
        var data = NSData.dataWithContentsOfFile_(filePath)
        if (data) {
            return NSJSONSerialization.JSONObjectWithData_options_error_(data, NSJSONReadingAllowFragments, null)
        }
        return null
    },
    saveJSON: (json, filePath) => {
        var data = NSJSONSerialization.dataWithJSONObject_options_error_(json, NSJSONWritingPrettyPrinted, null)
        data.writeToFile_atomically_(filePath, true)
    },
    toJSArray: (objcArray) => {
        var array = []
        objcArray.forEach(element => array.push(element))
        return array
    },
    /**
     * Show alert
     * @param {String} title
     * @param {(view)=>void} willShow
     * @param {(view)=>void} completion
     * @param {(view)=>void} cancel
     * @return {Number} response
     */
    alert: (title, willShow, completion, cancel) => {
        var alert = COSAlertWindow.new()
        title && alert.setMessageText(title)
        completion && alert.addButtonWithTitle("Ok")
        cancel && alert.addButtonWithTitle("Cancel")
        var view = NSView.alloc().initWithFrame(NSMakeRect(0, 0, 300, 100))
        view.setTranslatesAutoresizingMaskIntoConstraints_(true)
        alert.addAccessoryView(view)

        willShow(view)
        // Show the dialog
        switch (alert.runModal()) {
            case NSAlertFirstButtonReturn:
                if (completion) completion(view)
                else cancel && cancel(view)
                break;
            default:
                cancel && cancel(view)
                break;
        }
    },
    /**
     * Show alert
    * @param {String} title
    * @param {List} list
    * @param {(view,dropdown)=>void} willShow
    * @param {(view,dropdown,result,index)=>void} completion
    * @param {(view,dropdown)=>void} cancel
    * @return {Number} response
    */
    alertDropdown: (title, list, willShow, completion, cancel) => {
        var dropdown
        return DHAHelper.alert(
            title,
            (view) => {
                if (list) {
                    dropdown = NSPopUpButton.alloc().initWithFrame(NSMakeRect(0, 20, view.frame().size.width * 0.8, 22))
                    DHAHelper.toJSArray(list).forEach(element => dropdown.addItemWithTitle_(element.display))
                    view.addSubview(dropdown)
                }
                willShow(view, dropdown)
            },
            (view) => {
                dropdown
                    && completion(view, dropdown, dropdown.titleOfSelectedItem(), dropdown.indexOfSelectedItem())
                    || completion(view, null, null, null)
            },
            (view) => {
                dropdown
                    && cancel(view, dropdown)
                    || cancel(view, null)
            },
        )
    },
    log: (context, message) => {
        context.document.showMessage(message)
        log(message)
    }
}