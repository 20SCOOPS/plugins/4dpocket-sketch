@import 'dha-helper.js'

var DHALoader = {
  
  infoPath: context => DHAHelper.docDir(context, "hyky-info.json"),
  localePath: context => DHAHelper.docDir(context, "locale"),

  saveNewInfo: (context, info) => {
    var items = []
    NSFileManager.defaultManager().enumeratorAtURL_includingPropertiesForKeys_options_errorHandler(
      NSURL.fileURLWithPath_(DHALoader.localePath(context)),
      [NSURLNameKey, NSURLIsDirectoryKey],
      NSDirectoryEnumerationSkipsSubdirectoryDescendants,
      null
    ).allObjects().forEach(url => {
      var dataset = url.lastPathComponent()
      if (!String(dataset).startsWith(".")) {
        items.push({
          name: dataset,
          display: dataset.toUpperCase()
        })
      }
    })
    info.items = items
    DHAHelper.saveJSON(info, DHALoader.infoPath(context))
  },
  getInfo: context => {
    var info = DHAHelper.loadJSON(DHALoader.infoPath(context))
    return info && info.mutableCopy() || NSMutableDictionary.new()
  }
}