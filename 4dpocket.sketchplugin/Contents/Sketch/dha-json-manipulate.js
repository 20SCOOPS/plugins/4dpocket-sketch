@import 'dha-helper.js'

var DHAJSONManipulate = {
    //
    loadDataSet: (context, name) => {
        var samplePath = DHAHelper.docDir(context, `locale/${name}/text.json`)
        var data = NSData.dataWithContentsOfFile_(samplePath)
        var json = NSJSONSerialization.JSONObjectWithData_options_error_(data, NSJSONReadingAllowFragments, null)
        return json
    },

    //
    settingNodes: (data, nodes, level = 0) => {
        nodes.forEach(node => {
            DHAJSONManipulate.settingElementData(data, node)
            var nodeChildren = []
            node.children().forEach(n => nodeChildren.push(n))
            nodeChildren = nodeChildren.filter(n => n.objectID() != node.objectID())
            DHAJSONManipulate.settingNodes(data, nodeChildren, level + 1)
        })
    },

    //
    settingElementData: (data, node) => {
        var match = node.nodeName().match(/\{(.*)\}/)
        var keys = match && match[1]
        var values = keys && keys.split(",").map(key => data[key])
        if (!values) {
            return
        }
        if (node.class() == MSSymbolInstance) {
            var overrides = NSMutableDictionary.dictionaryWithDictionary_(node.overrides() || {})
            DHAHelper.toJSArray(node.availableOverrides())
            .filter(o => o.class() == MSAvailableOverride)
            .forEach((o, index) => {
                var point = String(o.overridePoint()).split("_")[0]
                overrides[point] = values[index] || "null"
            })
            node.setOverrides(overrides)
        }
        else if (node.class() == MSTextLayer) {
            node.setStringValue(values[0] || null);
        }
    }
}