@import 'dha-cmd.js'
@import 'dha-helper.js'

var checkAndUpdate = context => {
    var path = DHAHelper.pluginDir(context)

    // update plugins
    DHAHelper.log(context, "Checking for new updated of Plugins...")
    var result = DHACommand.updater(path)
    log(result)
    result = result.lastObject()
    if (Number(result) == 0) {
        DHAHelper.log(context, "up-to-date")
    }
    else if (Number(result) == 1) {
        DHAHelper.alert(
            "Found new updated. Wanna update?",
            (v) => {},
            (v) => {
                DHAHelper.log(context, "Updating...")
                var result = DHACommand.updater(path,"u").lastObject()

                if (Number(result) == 0) {
                    DHAHelper.log(context, "Update done.")
                }
                else {
                    DHAHelper.log(context, "Error please try again.")
                }
            },
            (v) => DHAHelper.log(context, "Cancel.")
        )
    }
    else {
        DHAHelper.log(context, "Error when check update.")
    }
}