var DHACommand = {
    /**
     * run command shell
     * @param {String} directory
     * @param {...String} args
     * @return {String[]} data logs
     */
    shell : (directory,...args)=>{
        return DHACommand.command("/bin/sh",directory,args)
    },
    /**
     * run Updater 
     * option u update plugin
     * option c check if have tools if not download it
     * option t update tools
     * option * check update plugin
     * @param {String} directory - container directory
     * @param {String} option
     * @return {String[]} data logs
     */
    updater : (directory,option="")=>{
        return DHACommand.shell(directory,"src/updater.sh",option)
    },
    /**
     * run Tools
     * honyak $platfom $temp_json $des $config
     * konyak $src_type $src $temp_json 
     * @param {Object} directory
     * @param {...String} args
     * @return {String[]} data logs
     */
    tools : (directory,...args)=>{
        return DHACommand.command(directory+"/src/tools",directory,args)
    },
    /**
     * run command git
     * @param {String} directory
     * @param {...String} args
     * @return {String[]} data logs
     */
    git : (directory,...args)=>{
        return DHACommand.command("/usr/bin/git",directory,args)
    },
    /**
     * remove command
     * @param {String} launchPath
     * @param {String} directory
     * @param {...String} args
     * @return {String[]} data logs
     */
    remove : (directory,...args) => {
        return DHACommand.command("/bin/rm",directory,args)
    },
    /**
     * run command any with array args
     * @param {String} launchPath
     * @param {String} directory
     * @param {String[]} args
     * @return {String[]} data logs
     */
    command : (launchPath,directory,args) => {
        var task = NSTask.new()
        var pipe = NSPipe.pipe()
        task.setLaunchPath_(launchPath)
        directory && task.setCurrentDirectoryPath_(directory)
        task.setArguments_(args)
        task.setStandardOutput_(pipe)
        task.launch()
        task.waitUntilExit()
        var dataLogs = pipe.fileHandleForReading().readDataToEndOfFile()
        var logs = NSString.alloc()
        .initWithData_encoding_(dataLogs,NSUTF8StringEncoding)
        .componentsSeparatedByString_("\n").mutableCopy()
        logs.removeObjectAtIndex_(logs.count()-1)
        return logs
    }
}